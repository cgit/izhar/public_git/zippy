# Maintained by the Fedora Desktop SIG:
# http://fedoraproject.org/wiki/SIGs/Desktop
# mailto:fedora-desktop-list@redhat.com

%include fedora-live-base.ks

timezone Asia/Kuala_Lumpur
repo --name=fedora --baseurl=file:///media/IMATION-EXT3/fedora-12/fedora/
repo --name=rpmfusion-free --baseurl=file:///media/IMATION-EXT3/fedora-12/rpmfusion-free/
repo --name=rpmfusion-nonfree --baseurl=file:///media/IMATION-EXT3/fedora-12/rpmfusion-free/

%packages
nss-mdns


# Moblin related packages
anerley
moblin*
bisho
mojito*
bognor-regis
bickley*
network-manager-netbook
uxlaunch
dalston
hornsey
nbtk
anjal
jana
telepathy-mission-control
telpathy-glib



# stuff

gdm

gstreamer-plugins-good
gstreamer-plugins-bad
gstreamer-ffmpeg
gstreamer-plugins-ugly


notification-daemon

-*devel

NetworkManager-openvpn
cnetworkmanager
NetworkManager-glib
terminator
elinks
wget
yum-plugin-presto
yum-plugin-fastestmirror
firefox
anaconda
libccss

# avoid weird case where we pull in more festival stuff than we need
festival
festvox-slt-arctic-hts

# dictionaries are big
-aspell-*
-hunspell-*
-man-pages*
-words

# save some space
-gnome-user-docs
-gimp-help
-gimp-help-browser
-gimp-data-extras
-evolution-help
-gnome-games
-gnome-games-help
-nss_db
-vino
-isdn4k-utils
-dasher
-evince-dvi
-evince-djvu
# not needed for gnome
-acpid

# these pull in excessive dependencies
-ekiga
-tomboy
-f-spot

# drop some system-config things
-system-config-boot
-system-config-language
-system-config-lvm
-system-config-network
-system-config-rootpassword
-system-config-services
-policycoreutils-gui
%end

%post
cat >> /etc/rc.d/init.d/livesys << EOF
# disable screensaver locking
#gconftool-2 --direct --config-source=xml:readwrite:/etc/gconf/gconf.xml.defaults -s -t bool /apps/gnome-screensaver/lock_enabled false >/dev/null
# set up timed auto-login for after 60 seconds
cat >> /etc/gdm/custom.conf << FOE
[daemon]
TimedLoginEnable=true
TimedLogin=liveuser
TimedLoginDelay=60
FOE

gconftool-2 --direct --config-source=xml:readwrite:/etc/gconf/gconf.xml.defaults -s -t bool /desktop/gnome/peripherals/touchpad/tap_to_click true > /dev/null

EOF

%end
